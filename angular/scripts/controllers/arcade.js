app.controller('dashCtrl', ['$scope','$http', function($scope,$http){
		$scope.highscore = "2212";

		$http.get('api/levels.json').then(function(res){
			$scope.levels = res.data.levels;
		})

	}]);
//questions page controller : crappy :(
	app.controller('questionsCtrl', ['$scope','$http', function($scope,$http){
		$scope.current = 1;

		$scope.userScore = 0;

		function randQuestion(){
			for(i=0; i<=$scope.questionsLength; i+=1){
				$scope.Question = $scope.allQuestions[$scope.randomNumber].question;
				$scope.Answer = $scope.allQuestions[$scope.randomNumber].answer;
				$scope.id = $scope.allQuestions[$scope.randomNumber].id;
				$scope.Options = $scope.allQuestions[$scope.randomNumber].options[0];			
				break;
			};//end for loop
		};//end function randQuestion()

			$scope.showHint = function(){
				swal({
	  			title: 'Hint !',
	  			text: 'Lorem Ipsum Dolor sit amet consecut laborum il eld meit',
	  			type:'info',
			});
			};

			$scope.pauseGame = function(){
				//also add the pause timer function here
				swal({
	  			title: 'Paused!',
	  			text: 'The game has been paused || any other pause msg',
	  			type:'warning',
	  			confirmButtonText: "<i class='mdi-av-play-arrow i-24' tooltip-placement='bottom' tooltip-animation='true' tooltip='Continue'></i>",
			});
			};
		//retrieving data from questions JSON file.....here i used all my vars in the $http method because it's rough work...
		$http.get('api/questions.json').then(function(res){
			$scope.allQuestions =  res.data.questions;
			$scope.questionsLength = $scope.allQuestions.length;

			$scope.randomNumber = Math.floor(Math.random()*$scope.questionsLength);

			randQuestion();

			});	

	}]);

//main menu controller...
	app.controller('menuCtrl', ['$scope', function($scope){
		$scope.appName = "Arcade"

		$scope.showInfo = function(){
			swal({
	  			title: 'Instructions!',
	  			text: 'Dummy Text serving as the info',
	  			type:'info',
			})
		};

		$scope.showHighscore = function(){
			swal({
	  			title: 'Highscore!',
	  			text: '<h3 class="text-info">20201</h3>',
	  			type:'info',
			})
		};

		$scope.closeApp = function(){
			swal({
				  title: "Are you sure you want to quit?",
				  text: "You will not be able to recover this imaginary file!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes quit!",
				  cancelButtonText: "No",
				  closeOnConfirm: false,
				  closeOnCancel: false
				},
				function(isConfirm){
				  if (isConfirm) {
				    swal("Closed!", "The app's message shows here.", "success");
				  } else {
				    swal("Cancelled", "Nice work, play on :)", "error");
				  }
				});
		};
	}])


