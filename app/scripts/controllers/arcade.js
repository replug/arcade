app.controller('dashCtrl', ['$scope','$http', function($scope,$http){
		$scope.highscore = "2212";

		$http.get('api/levels.json').then(function(res){
			$scope.levels = res.data.levels;
		})

	}]);
//questions page controller
app.controller('questionsCtrl', ['$scope','$http','$rootScope','quizHandler', function($scope,$http,$rootScope,quizHandler){
	$scope.currentQuestion = 1;
	$scope.userScore = 0;

	$rootScope.showAlert = function(type,msg,title){
	swal({
		type: type,
		text: msg,
		title: title
	});
};

	$scope.userScore = 0;

		$scope.showHint = function(){
			swal({
  			title: 'Hint !',
  			text: 'Lorem Ipsum Dolor sit amet consecut laborum il eld meit',
  			type:'info',
		});
		};

		$scope.pauseGame = function(){
			//also add the pause timer function here
			swal({
  			title: 'Paused!',
  			text: '',
  			type:'warning',
  			confirmButtonText: "<i class='mdi-av-play-arrow i-24' tooltip-placement='bottom' tooltip-animation='true' tooltip='Continue'></i>",
		});
		};

	//retrieving data from questions JSON file...
	$http.get('api/questions.json').then(function(res){
		$scope.allQuestions =  quizHandler.shuffle(res.data.questions);
		
		$scope.itemsPerPage = 1;

		$scope.questionsLength = $scope.allQuestions.length;

		$scope.next = function(index){
			if(index > 0 && index <= $scope.allQuestions.length){
				$scope.currentQuestion = index;
			}
		};
		$scope.$watch('currentQuestion + 1', function(){
        	var begin = (($scope.currentQuestion - 1) * $scope.itemsPerPage),
        	end = begin + $scope.itemsPerPage;

        	$scope.filteredQuestions = $scope.allQuestions.slice(begin, end);
        });

        $scope.checkAns = function(qid,oid,index){
        	//var i;
			for (var i = $scope.allQuestions.length - 1; i >= 0; i--) {
				//console.log($scope.allQuestions[i].id);
				if(qid == $scope.allQuestions[i].id){
					//console.log($scope.allQuestions[i].id + ' ' + qid);
					if(oid == $scope.allQuestions[i].answer){
						//feedback alert method from the arcade service
						quizHandler.feedbackAlert('Good Job!','You picked the right answer!','thumbs-up.jpg',index);
						$scope.userScore += 1;
						console.log("right ans picked");
					}else{
						quizHandler.feedbackAlert('Oops Sorry!','You picked the wrong answer','wrong.png',index);
						console.log("wrong ans picked");
					}
				}
			}
        }

		});	

	$scope.timerHandler = function(examTime){
			function getTimeRemaining(endtime) {
  				var t = Date.parse(endtime) - Date.parse(new Date());
				var seconds = Math.floor((t / 1000) % 60);
				var minutes = Math.floor((t / 1000 / 60) % 60);
				var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
				var days = Math.floor(t / (1000 * 60 * 60 * 24));
				return {
				  'total': t,
				  'days': days,
				  'hours': hours,
				  'minutes': minutes,
				  'seconds': seconds
				};
			};

		function initializeClock(id, endtime) {
			//var hours = document.getElementById('hours');
  			var minutes = document.getElementById('minutes');
  			var seconds = document.getElementById('seconds');

  			function updateClock() {
    			var t = getTimeRemaining(endtime);

    			//hours.innerHTML = ('0' + t.hours).slice(-2);
    			minutes.innerHTML = ('0' + t.minutes).slice(-2);
    			seconds.innerHTML = ('0' + t.seconds).slice(-2);

    			if (t.total <= 0) {
      				clearInterval(timeinterval);
      				$rootScope.showAlert('warning','Oops you ran out of time','Time Up');
      				console.log('timeUp');
    			} else{
    				//console.log(t.total-t.timeRemaining);
    			};
  			};

  		updateClock();
  		var timeinterval = setInterval(updateClock, 1000);
		};

		var deadline = new Date(Date.parse(new Date()) + examTime * 60 * 1000);
		console.log(deadline);
		return initializeClock('clockdiv', deadline);
	};

	$scope.timerHandler(30); //used 60 for testing....time is measured in minutes...60 means 60minutes means....time could be associated with each level
	/*time could also be stored in localstorage then passed as a param into the timeHandler function
	 ion know.....got too many ideas on how we could do this ....*/
}]);

//main menu controller...
app.controller('menuCtrl', ['$scope', function($scope){
	$scope.appName = "Arcade"

	$scope.showInfo = function(){
		swal({
  			title: 'Instructions!',
  			text: 'Dummy Text serving as the info',
  			type:'info',
		})
	};

	$scope.showHighscore = function(){
		swal({
  			title: 'Highscore!',
  			text: '<h3 class="text-info">20201</h3>',
  			type:'info',
		})
	};

	$scope.closeApp = function(){
		swal({
			  title: "Are you sure you want to quit?",
			  text: "Unsaved data would be lost!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Yes quit!",
			  cancelButtonText: "No",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm){
			  if (isConfirm) {
			    swal("Closed!", "The app's message shows here.", "success");
			  } else {
			    swal("Cancelled", "Nice work, play on :)", "error");
			  }
			});
	};
}])


