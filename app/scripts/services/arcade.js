/**
* arcade question service
*/
app.service('quizHandler', function(){
	//shuffle method
    this.shuffle = function(array){
        var length = array.length, temp, randomNumber;
        while(0 != length){
            randomNumber = Math.floor(Math.random() * length);
            length -= 1;
            temp = array[length];
            array[length] = array[randomNumber];
            array[randomNumber] = temp;
            // works like the swap algorithm: temp=a, a=b, b=temp
        };
        return array;
    };
    this.feedbackAlert = function(title,text,imageName,index){
       var alert = swal({
            title: title,
            text: text,
            imageHeight: '90',
            imageWidth: '90',
            allowEscapeKey: false,
            allowOutsideClick: false,
            showCancelButton: true,
            imageUrl: 'images/' + imageName,
            cancelButtonText: "Show Explanation",
            confirmButtonText: "Continue"
        }, function(isConfirm){   
                    if (isConfirm) {     
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                    } else {     
                        swal("Cancelled", "Your imaginary file is safe :)", "error");   
                    } 
                });

       return alert;
    }
});